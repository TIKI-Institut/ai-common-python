# Changelog

## 0.7.0

Added publishing of dynamic qos entries

## 0.6.1

Removed uuid package dependency

## 0.6.0

Adjust Webservice, to use waitress as wsgi server.

## 0.5.1

introducing setup logger from env var

## 0.5.1

Extended QoS save_adhoc_score to support jupyter noteboook.

## 0.5.0

Added digitalization workbench REST client

## 0.4.8

Implemented save_adhoc_score method for storing QoS scores from jobs. 

Added parameters property to QoS result. 

## 0.4.6

removed duplicated code on web / job kernel startup

## 0.4.5

Adjust Webservice, such that JWT_ISSUER must not be set or can be set to empty string. 

## 0.4.4

Adjust Webservice, such that JWT_ISSUER must not be set. However if secure endpoints are used and no JWT_ISSUER env is set, then error will occur.

## 0.4.3

Remove lowercase cast in webservice endpoint.

## 0.4.2

Fixing serving of static content in flask application


## 0.4.1

Fixing mqtt connection lost. https://tiki-institut.atlassian.net/browse/TIKIDSP-1440

## 0.4.0

QOS integration. https://tiki-institut.atlassian.net/browse/TIKIDSP-1346


## 0.3.0

Removed direct dependency of python kernels to ai-common-python.
https://tiki-institut.atlassian.net/browse/TIKIDSP-893
