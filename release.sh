#!/usr/bin/env bash

# https://packaging.python.org/tutorials/packaging-projects/#uploading-the-distribution-archives

pip.exe install twine

python.exe setup.py sdist bdist_wheel
python.exe -m twine upload --repository-url=https://nexus.tiki-dsp.io/repository/tiki-pypi/ dist/*