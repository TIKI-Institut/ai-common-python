#!/usr/bin/env bash

# https://packaging.python.org/tutorials/packaging-projects/#uploading-the-distribution-archives

#pip install twine

# TODO https://github.com/pypa/twine/issues/230
# when this gets done, we can put the information of repository to the pyproject.toml

# clear dist directory, otherwise everything will be published
rm -R dist

python setup.py sdist bdist_wheel
# get .pypirc config file from keepass and save it under $HOME/.pypirc
python -m twine upload --repository=tikipypidev dist/*
# for interactively setting username and password
#python -m twine upload --repository-url=https://nexus.tiki-dsp.io/repository/tiki-pypi/ dist/*
