# ai-common

This library contains code for hosting python applications on the TIKI Data Science Platform

## Compile ai-common
To import dependencies so that project can be compiled and tested, execute in terminal within IDE:
```shell script
pip install -r requirements.txt
```


## Install library

### Over TIKI PyPi repository

**Recommended**. python version specificier can be used => hotfix release will be retrieved automatically with "~="

#### requirements.txt
```shell script
--extra-index-url https://nexus.tiki-dsp.io/repository/tiki-pypi/simple
ai-common~=0.4.1
```

### Dev / CI releases

Created from continuous integration system after every commit

#### requirements.txt
```shell script
--extra-index-url https://nexus.tiki-dsp.io/repository/tiki-pypi-dev/simple
ai-common
```

### Over VCS

**Note**: You will get potential unstable code directly over VCS 

Any branch, tag or commit revision can be selected.

#### requirements.txt
```shell script
git+https://bitbucket.org/TIKI-Institut/ai-common-python.git@master#egg=ai-common
```

### How To Release a Version / Tag

[PEP-440 Version standard](https://www.python.org/dev/peps/pep-0440/#version-scheme)

Examples:
- Normal release => 0.4.1
- Dev Release => 0.2.dev0 (like 0.2-SNAPSHOT in maven)

see [Specs](https://tiki-institut.atlassian.net/wiki/spaces/DA/pages/1304231951/Release+Versioning+concept)

- update version in setup.py
- create a tag with this version
- increase / change the version in master branch
- trigger jenkins tag build for publishing to pipy
