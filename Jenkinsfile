pipeline {
    agent { kubernetes { label 'dsp-python-310' } }

    options {
        // do not clean TAG build logs
        buildDiscarder(env.TAG_NAME == null ? logRotator(daysToKeepStr: '30') : null)
        // ensures that automated branch indexing doesn't trigger branch job builds,
        // but also destroys the its feature to trigger on bitbucket commit push
        // solution: additional bitbucketPush trigger below
        overrideIndexTriggers(false)
    }

    triggers {
        // ensure that the branch job itself gets triggered on bitbucket commit push
        bitbucketPush()
    }

    stages {
        stage('Prepare') {
            steps {
                bitbucketStatusNotify(buildState: 'INPROGRESS')
            }
        }        
        stage('Build') {
            steps {
                container('python310') {
                    sh '''pip install .'''
                }
            }
        }
        stage('Test'){
            steps {
                container('python310') {
                    sh '''python -m unittest'''
                }
            }
        }
        stage('Release Master / Dev') {
            when {
                branch 'master'
            }
            steps {
                container('python310') {
                    sh "chmod +x ./publish-dev-release.sh"
                    sh "./publish-dev-release.sh"
                }
            }
        }
        stage('Release Tag') {
            when {
                tag ''
            }
            steps {
                container('python310') {
                    sh "chmod +x ./publish-final-release.sh"
                    sh "./publish-final-release.sh"
                }
            }
        }
    }

    post {
        failure {
            notifyViaSlack("#FF0000")
            bitbucketStatusNotify(buildState: 'FAILED')
        }
        fixed {
            notifyViaSlack("#00FF00")
            bitbucketStatusNotify(buildState: 'SUCCESSFUL')
        }
    }
}

def notifyViaSlack(String colorCode) {
  def subject = "${currentBuild.currentResult}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
  def summary = "${subject} ${env.BUILD_URL}"

  slackSend (color: colorCode, message: summary)
}
