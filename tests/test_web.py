import unittest

from ai_common import WEB_RESOURCES_STORE, WEB_RESOURCE_KEY_ENDPOINT, WEB_RESOURCE_KEY_FUNCTION, \
    WEB_RESOURCE_KEY_METHODS, WEB_APPLICATION_STARTUP_FUNCTION_STORE


class WebDecoratorTest(unittest.TestCase):
    def setUp(self) -> None:
        # this ensures all needed codes is loaded
        # noinspection PyUnresolvedReferences
        import tests.examples.web

    def test_fetching_of_web_decorators(self):
        test_roles = {'a': [],
                      'b': None,
                      'c': ["web_role"],
                      'd': ["should_be_registered_even_if_not_exported"],
                      'e': ["web_role_2"],
                      'f': ["duplicate_roles"]}

        for x in ['a', 'b', 'c', 'd', 'e', 'f']:
            store = WEB_RESOURCES_STORE['/' + x]
            self.assertEqual(store[WEB_RESOURCE_KEY_ENDPOINT], '/' + x)
            self.assertEqual(store[WEB_RESOURCE_KEY_FUNCTION] is None, False)
            self.assertEqual(store[WEB_RESOURCE_KEY_METHODS], ['GET'])

    def test_fetching_of_web_application_startup_decorators(self):
        store = WEB_APPLICATION_STARTUP_FUNCTION_STORE
        self.assertEqual(len(store), 1)
