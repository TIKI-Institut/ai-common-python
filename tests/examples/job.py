import click

from ai_common.decorators import job


@job("test-job")
def job1() -> int:
    return 0


@job("test-job-2", ["job_role_1"])
def job2() -> int:
    return 0


@job("test-job-3", ["duplicate_roles"])
def job3() -> int:
    return 0


@job("test-job-with-parameter")
@click.option("--param1")
@click.option("--param2")
def job_with_parameter(param1, param2):
    return 0
