from ai_common.decorators import web_application_startup
from ai_common.decorators import web_resource


@web_resource("/a", [])
def job1() -> int:
    return 0


@web_resource("/b", None)
def job2() -> int:
    return 0


@web_resource("/c", ["web_role"])
def job3() -> int:
    return 0


@web_resource("/d", ["should_be_registered_even_if_not_exported"])
def job4() -> int:
    return 0


@web_resource("/e", ["web_role_2"])
def job5() -> int:
    return 0


@web_resource("/f", ["duplicate_roles"])
def job3() -> int:
    return 0


@web_application_startup()
def on_application_startup():
    return
