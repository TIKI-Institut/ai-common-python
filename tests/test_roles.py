import tempfile
import unittest

from ai_common.auth import AUTH_STORE, AUTH_KEY_ROLES
from ai_common.decorators.job import list_roles
from ai_common import dump_roles


class RolesTest(unittest.TestCase):
    def setUp(self) -> None:
        # this ensures all needed codes is loaded
        # noinspection PyUnresolvedReferences
        import tests.examples.web
        # noinspection PyUnresolvedReferences
        import tests.examples.job
        # noinspection PyUnresolvedReferences
        import tests.examples.job_a
        # noinspection PyUnresolvedReferences
        import tests.examples.job_b
        # noinspection PyUnresolvedReferences
        import tests.examples.job_c

    def test_dump_roles(self):
        json_data = dump_roles()

        self.assertEqual(json_data.startswith("{\"roles\": ["), True)

    def test_roles_registration(self):
        self.assertEqual(6, len(AUTH_STORE[AUTH_KEY_ROLES]), 'The Correct amount of roles should be fetched')

        self.assertIn("web_role", AUTH_STORE[AUTH_KEY_ROLES],
                      'The Correct roles should be fetched')
        self.assertIn("should_be_registered_even_if_not_exported", AUTH_STORE[AUTH_KEY_ROLES],
                      'The Correct roles should be fetched')
        self.assertIn("web_role_2", AUTH_STORE[AUTH_KEY_ROLES],
                      'The Correct roles should be fetched')
        self.assertIn("duplicate_roles", AUTH_STORE[AUTH_KEY_ROLES],
                      'The Correct roles should be fetched')
        self.assertIn("job_role_1", AUTH_STORE[AUTH_KEY_ROLES],
                      'The Correct roles should be fetched')

    def test_role_enlistment(self):
        self.assertEqual(6, len(AUTH_STORE[AUTH_KEY_ROLES]), 'The Correct amount of roles should be fetched')

        _, roles_file_path = tempfile.mkstemp(prefix="roles", suffix=".json", text=True)
        list_roles(roles_file_path)

        with open(roles_file_path, "r") as text_file:
            json_output = text_file.read()

            self.assertIn("duplicate_roles", json_output)


