import asyncio
import json
import logging
import os
import signal
from unittest import TestCase

from ai_common.mqtt.mqttClient import IMqttClient, MqttQos
from ai_common.qos.cmd.qosCommand import QosCommand
from ai_common.qos.model.flavorReference import DSP_PRINCIPAL_KEY, DSP_NAME_KEY
from ai_common.qos.model.qosRunResult import QosRunStatus

logger = logging.getLogger(__name__)

STOP = asyncio.Event()


def ask_exit():
    STOP.set()


class MqttClientMock(IMqttClient):
    msg: str

    async def connect(self):
        logger.info("connect")

    def publish(self, topic: str, msg: str, qos: MqttQos = MqttQos.AT_LEAST_ONCE):
        logger.info("publish")
        self.msg = msg

    async def disconnect(self):
        logger.info("disconnect")


class TestQosCommand(TestCase):

    def setUp(self):
        os.environ[DSP_PRINCIPAL_KEY] = "tiki-test"
        os.environ[DSP_NAME_KEY] = "test-project"

    def tearDown(self):
        if os.name != 'nt':
            loop = asyncio.get_event_loop()
            loop.add_signal_handler(signal.SIGINT, ask_exit)
            loop.add_signal_handler(signal.SIGTERM, ask_exit)

        del os.environ[DSP_PRINCIPAL_KEY]

    def test_run_qos_async_job(self):
        mqtt_client = MqttClientMock()
        cmd = QosCommand(mqtt_client)
        loop = asyncio.get_event_loop()
        loop.run_until_complete(cmd.run_qos_async_job(1, "test-qos", lambda: 3.2, 'params'))
        self.assertEqual(json.loads(mqtt_client.msg)["status"], QosRunStatus.SUCCESS.value)
        self.assertEqual(json.loads(mqtt_client.msg)["score"], 3.2)
        self.assertEqual(json.loads(mqtt_client.msg)["qosEntry"]["name"], "test-qos")

    def invalid_job(self):
        raise Exception("some error")

    def test_run_invalid_qos_async_job(self):
        mqtt_client = MqttClientMock()
        cmd = QosCommand(mqtt_client)
        loop = asyncio.get_event_loop()
        loop.run_until_complete(cmd.run_qos_async_job(1, "test-qos", self.invalid_job, 'params'))
        self.assertEqual(json.loads(mqtt_client.msg)["qosEntry"]["name"], "test-qos")
        self.assertEqual(json.loads(mqtt_client.msg)["status"], QosRunStatus.FAILED.value)
        self.assertEqual(json.loads(mqtt_client.msg)["score"], None)

    def test_run_qos_async_job_entry_not_found(self):
        mqtt_client = MqttClientMock()
        cmd = QosCommand(mqtt_client)
        loop = asyncio.get_event_loop()
        loop.run_until_complete(cmd.run_qos_async_job(1, "test-qos", None, 'params'))
        self.assertEqual(json.loads(mqtt_client.msg)["qosEntry"]["name"], "test-qos")
        self.assertEqual(json.loads(mqtt_client.msg)["status"], QosRunStatus.NOT_FOUND.value)
        self.assertEqual(json.loads(mqtt_client.msg)["score"], None)

    def test_register_async_endpoint(self):
        mqtt_client = MqttClientMock()
        cmd = QosCommand(mqtt_client)
        loop = asyncio.get_event_loop()
        loop.run_until_complete(cmd.register_async_endpoint(["test-entry"]))
        self.assertEqual(json.loads(mqtt_client.msg)["entries"][0]["name"], "test-entry")
