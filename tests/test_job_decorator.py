from unittest import TestCase

from ai_common.decorators.job import async_qos_entry, JOB_STORE, JOB_KEY_QOS_FUNCTION


@async_qos_entry("qos-test1")
def qos1():
    return 1.1


@async_qos_entry("qos-test2")
def qos2():
    return 2.2


class TestQosCommand(TestCase):

    def test_job(self):
        self.assertEqual(len(JOB_STORE[JOB_KEY_QOS_FUNCTION]), 2, "It should be 2 qos entries in JOB_STORE.")
        self.assertEqual(JOB_STORE[JOB_KEY_QOS_FUNCTION]["qos-test1"](), 1.1,
                         "Stored function should return correct result.")
