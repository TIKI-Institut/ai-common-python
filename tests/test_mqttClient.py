import os
from unittest import TestCase

from ai_common.mqtt.mqttClient import MqttClient


class TestMqttClient(TestCase):

    def test_init(self):
        os.environ['NAMESPACE_BROKER_SERVICE_PORT'] = "1883"
        print(os.environ[MqttClient.PORT_ENV])
        mqtt_client = MqttClient()
        self.assertEqual(mqtt_client.broker_host_name, "namespace-broker")
        self.assertEqual(mqtt_client.broker_port, "1883")

