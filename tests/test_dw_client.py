import os
import shutil
import unittest

import responses

from ai_common import DWClient

expected_token_string = 'raw_access_token'


def ephemeral_directory():
    return os.path.join(os.path.curdir, 'data')


def prepare() -> str:
    data_dir = ephemeral_directory()
    token_file = os.path.join(data_dir, 'token.txt')
    if not os.path.exists(data_dir):
        os.mkdir(data_dir)
        with open(token_file, 'x') as handle:
            handle.write(expected_token_string)
    return token_file


def clean():
    data_dir = ephemeral_directory()
    shutil.rmtree(data_dir)


class DWClientTest(unittest.TestCase):

    @responses.activate
    def test_fs(self):
        responses.add(responses.GET, "http://some-url/fs", json=['file', 'pvc', 'hdfs'], status=200)

        token_file = prepare()

        client = DWClient('http://some-url', token_file)

        fs_result = client.fs()

        self.assertEqual(3, fs_result.__len__())

        clean()

    @responses.activate
    def test_fs_bad_request(self):
        responses.add(responses.GET, "http://some-url/fs", json=['file', 'pvc', 'hdfs'], status=400)

        token_file = prepare()

        client = DWClient('http://some-url', token_file)

        fs_result = client.fs()

        self.assertEqual(None, fs_result)

        clean()

    @responses.activate
    def test_list(self):
        responses.add(responses.POST, "http://some-url/list",
                      json=[{"name": "elolist", "size": 2, "isDir": True, "fsType": "file", "lastModified": 1625655259},
                            {"name": "elolist2", "size": 4, "isDir": True, "fsType": "file",
                             "lastModified": 1625655259}], status=200)

        token_file = prepare()

        client = DWClient('http://some-url', token_file)

        result = client.list('file:/')

        self.assertEqual(2, result.__len__())

        clean()

    @responses.activate
    def test_delete(self):
        responses.add(responses.POST, "http://some-url/delete", status=200)

        token_file = prepare()

        client = DWClient('http://some-url', token_file)

        result = client.delete('file:/')

        self.assertEqual(True, result)

        clean()

    @responses.activate
    def test_copy(self):
        responses.add(responses.POST, "http://some-url/copy",
                      json={"taskId": 6, "operation": "MOVE", "status": "DONE", "from_": "hdfs:/text.txt",
                            "to": "hdfs:/new.txt", "overwrite": "True", "username": "tiki-tfrischholz", "principal": "tiki-test",
                            "created": 1627297701}, status=200)

        token_file = prepare()

        client = DWClient('http://some-url', token_file)

        result = client.copy('file:/', 'file:/B', True, True)

        self.assertEqual(6, result.task_id)
        self.assertEqual('MOVE', result.operation)
        self.assertEqual('DONE', result.status)
        self.assertEqual('hdfs:/text.txt', result.from_)
        self.assertEqual('hdfs:/new.txt', result.to)
        self.assertEqual('True', result.overwrite)
        self.assertEqual('tiki-tfrischholz', result.username)
        self.assertEqual('tiki-test', result.principal)
        self.assertEqual(1627297701, result.created)

        clean()

    @responses.activate
    def test_move(self):
        responses.add(responses.POST, "http://some-url/move",
                      json={"taskId": 6, "operation": "MOVE", "status": "DONE", "from_": "hdfs:/text.txt",
                            "to": "hdfs:/new.txt", "overwrite": "True", "username": "tiki-tfrischholz", "principal": "tiki-test",
                            "created": 1627297701}, status=200)

        token_file = prepare()

        client = DWClient('http://some-url', token_file)

        result = client.move(from_='file:/', to='file:/B', overwrite=True, wait=True)

        self.assertEqual(6, result.task_id)
        self.assertEqual('MOVE', result.operation)
        self.assertEqual('DONE', result.status)
        self.assertEqual('hdfs:/text.txt', result.from_)
        self.assertEqual('hdfs:/new.txt', result.to)
        self.assertEqual('True', result.overwrite)
        self.assertEqual('tiki-tfrischholz', result.username)
        self.assertEqual('tiki-test', result.principal)
        self.assertEqual(1627297701, result.created)

        clean()

    @responses.activate
    def test_task_status(self):
        responses.add(responses.GET, "http://some-url/task/6",
                      json={"taskId": 6, "operation": "MOVE", "status": "DONE", "from_": "hdfs:/text.txt",
                            "to": "hdfs:/new.txt", "overwrite": "True", "username": "tiki-tfrischholz", "principal": "tiki-test",
                            "created": 1627297701}, status=200)

        token_file = prepare()

        client = DWClient('http://some-url', token_file)

        result = client.task_status(6)

        self.assertEqual(6, result.task_id)
        self.assertEqual('MOVE', result.operation)
        self.assertEqual('DONE', result.status)
        self.assertEqual('hdfs:/text.txt', result.from_)
        self.assertEqual('hdfs:/new.txt', result.to)
        self.assertEqual('True', result.overwrite)
        self.assertEqual('tiki-tfrischholz', result.username)
        self.assertEqual('tiki-test', result.principal)
        self.assertEqual(1627297701, result.created)

        clean()

    @responses.activate
    def test_mkdir(self):
        responses.add(responses.POST, "http://some-url/mkdir", status=200)

        token_file = prepare()

        client = DWClient('http://some-url', token_file)

        result = client.mk_dir('file:/some-dir')

        self.assertEqual(True, result)

        clean()

    @responses.activate
    def test_download(self):
        token_file = prepare()

        with open(token_file, 'r') as handle:
            responses.add(responses.POST, "http://some-url/download", status=200,
                          body=bytes(str.encode(handle.read())))

        client = DWClient('http://some-url', token_file)

        result = client.download('file:/some-file.txt')

        self.assertEqual(str.encode(expected_token_string), result)

        clean()

    @responses.activate
    def test_upload(self):
        responses.add(responses.POST, "http://some-url/upload", status=200)

        token_file = prepare()

        client = DWClient('http://some-url', token_file)

        result = client.upload('file:/token.txt', token_file)

        self.assertEqual(True, result)

        clean()
