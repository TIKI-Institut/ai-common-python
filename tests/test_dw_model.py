import unittest

from ai_common import ListResult, TaskResult


class DWModelTest(unittest.TestCase):

    def test_list_result_json_parsing(self):

        json_str = """
            {"name":"elolist","size":2,"isDir":true,"fsType":"file","lastModified":1625655259}
        """

        obj = ListResult.from_json(json_str)
        self.assertEqual(obj.name, 'elolist')
        self.assertEqual(obj.is_dir, True)
        self.assertEqual(obj.fs_type, 'file')
        self.assertEqual(obj.last_modified, 1625655259)

    def test_task_result_json_parsing(self):

        json_str = """
            {"taskId":6,"operation":"MOVE","status":"DONE","from_":"hdfs:/text.txt","to":"hdfs:/new.txt",
            "overwrite": "True","username":"tiki-tfrischholz","principal":"tiki-test","created":1627297701}
        """

        obj = TaskResult.from_json(json_str)
        self.assertEqual(obj.task_id, 6)
        self.assertEqual(obj.operation, 'MOVE')
        self.assertEqual(obj.status, 'DONE')
        self.assertEqual(obj.from_, 'hdfs:/text.txt')
        self.assertEqual(obj.to, 'hdfs:/new.txt')
        self.assertEqual(obj.overwrite, 'True')
        self.assertEqual(obj.username, 'tiki-tfrischholz')
        self.assertEqual(obj.principal, 'tiki-test')
        self.assertEqual(obj.created, 1627297701)

