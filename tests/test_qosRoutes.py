import os
import json
import logging
from unittest import TestCase

import flask

from ai_common.decorators.web import sync_qos_entry
from ai_common.qos.model.flavorReference import DSP_PRINCIPAL_KEY, DSP_NAME_KEY
from ai_common.qos.model.qosRunResult import QosRunStatus
from ai_common.qos.qosRoutes import qos_run_endpoint, qos_list_endpoint

logger = logging.getLogger(__name__)


@sync_qos_entry("web-qos-test")
def qos():
    return 1.1


class TestQosEndpoint(TestCase):

    def setUp(self):
        os.environ[DSP_PRINCIPAL_KEY] = "tiki-test"
        os.environ[DSP_NAME_KEY] = "test-project"

    def tearDown(self):
        del os.environ[DSP_PRINCIPAL_KEY]
        del os.environ[DSP_NAME_KEY]

    def test_list(self):
        web_entries = qos_list_endpoint()
        self.assertEqual(json.loads(web_entries)["entries"][0]["name"], "web-qos-test")
        self.assertEqual(json.loads(web_entries)["entries"][0]["type"], "Sync")

    def test_run(self):
        app = flask.Flask(__name__)
        with app.test_request_context("/?transactionId=23&name=web-qos-test"):
            run_result = qos_run_endpoint()
            self.assertEqual(json.loads(run_result)["status"], QosRunStatus.SUCCESS.value)
            self.assertEqual(json.loads(run_result)["score"], 1.1)
            self.assertEqual(json.loads(run_result)["id"], 23)

    def test_run_invalid_transaction_id(self):
        app = flask.Flask(__name__)
        with app.test_request_context("/?transactionId=invalid&name=web-qos-test"):
            msg, code = qos_run_endpoint()
            self.assertEqual(msg, "Invalid transactionId: invalid")
            self.assertEqual(code, 400)

    def test_run_missing_transaction_id(self):
        app = flask.Flask(__name__)
        with app.test_request_context("/?name=web-qos-test"):
            msg, code = qos_run_endpoint()
            self.assertEqual(msg, "Missing transactionId request parameter")
            self.assertEqual(code, 400)

    def test_run_missing_name(self):
        app = flask.Flask(__name__)
        with app.test_request_context("/?transactionId=1"):
            msg, code = qos_run_endpoint()
            self.assertEqual(msg, "Missing name request parameter")
            self.assertEqual(code, 400)

    def test_run_missing_params(self):
        app = flask.Flask(__name__)
        with app.test_request_context("/"):
            msg, code = qos_run_endpoint()
            self.assertEqual(msg, "Missing transactionId request parameter")
            self.assertEqual(code, 400)
