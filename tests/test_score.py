import json
import os
import unittest

import ai_common.qos.score as score
from ai_common.qos.model.flavorReference import DSP_PRINCIPAL_KEY, DSP_NAME_KEY, DSP_ENVIRONMENT_KEY, \
    DSP_FLAVOR_NAME_KEY, \
    DSP_FLAVOR_VERSION_KEY, FlavorReference
from ai_common.qos.model.qosRunResult import QosRunStatus
from tests.test_qosCommand import MqttClientMock


class ScoreTest(unittest.TestCase):

    def test_publish_dynamic_score_from_job(self):
        mqtt_client = MqttClientMock()
        ScoreTest._job_environ()
        score._mqtt_client = mqtt_client
        score.save_dynamic_score("entry-name", 0.27)
        self.assertEqual(json.loads(mqtt_client.msg)["status"], QosRunStatus.SUCCESS.value)
        self.assertEqual(json.loads(mqtt_client.msg)["score"], 0.27)
        self.assertEqual(json.loads(mqtt_client.msg)["qosEntry"]["name"], "entry-name")
        self._assert_flavor_reference(mqtt_client)

    def test_publish_dynamic_score_from_jupyterhub(self):
        mqtt_client = MqttClientMock()
        ScoreTest._jupyterhub_environ()
        score._mqtt_client = mqtt_client
        model_params = {
            "C": 1.5,
            "kernel": "linear",
            "gamma": 2.5
        }
        score.save_dynamic_score("jupyter-score", 0.28, json.dumps(model_params))
        self.assertEqual(json.loads(mqtt_client.msg)["status"], QosRunStatus.SUCCESS.value)
        self.assertEqual(json.loads(mqtt_client.msg)["score"], 0.28)
        self.assertEqual(json.loads(mqtt_client.msg)["qosEntry"]["name"], "jupyter-score")
        flavor = FlavorReference()
        flavor.principal = "tiki-test"
        flavor.name = "test-user"
        flavor.environment = ""
        flavor.flavorName = ""
        flavor.flavorVersion = ""
        self.assertEqual(json.loads(mqtt_client.msg)["flavorReference"], flavor.__dict__)

    def test_publish_dynamic_score_from_jupyterhub_with_flavor_name_and_version(self):
        mqtt_client = MqttClientMock()
        # all needed env vars are in job already set
        ScoreTest._jupyterhub_environ()
        score._mqtt_client = mqtt_client
        # loop = asyncio.get_event_loop()
        score.save_dynamic_score("test-jupyter-with-flavor", 0.35, flavor_name="some-flavor",
                                 flavor_version="some-version")
        self.assertEqual(json.loads(mqtt_client.msg)["status"], QosRunStatus.SUCCESS.value)
        self.assertEqual(json.loads(mqtt_client.msg)["score"], 0.35)
        self.assertEqual(json.loads(mqtt_client.msg)["qosEntry"]["name"], "test-jupyter-with-flavor")
        flavor = FlavorReference()
        flavor.principal = "tiki-test"
        flavor.name = "test-user"
        flavor.environment = ""
        flavor.flavorName = "some-flavor"
        flavor.flavorVersion = "some-version"
        self.assertEqual(json.loads(mqtt_client.msg)["flavorReference"], flavor.__dict__)

    @staticmethod
    def _jupyterhub_environ():
        os.environ[DSP_PRINCIPAL_KEY] = "tiki-test"
        os.environ[DSP_NAME_KEY] = "test-user"
        ScoreTest._delete_env(DSP_ENVIRONMENT_KEY)
        ScoreTest._delete_env(DSP_FLAVOR_NAME_KEY)
        ScoreTest._delete_env(DSP_FLAVOR_VERSION_KEY)

    @staticmethod
    def _delete_env(name: str):
        if os.getenv(name):
            del os.environ[name]

    @staticmethod
    def _job_environ():
        os.environ[DSP_PRINCIPAL_KEY] = "tiki-test"
        os.environ[DSP_NAME_KEY] = "test-project"
        os.environ[DSP_ENVIRONMENT_KEY] = "test-env"
        os.environ[DSP_FLAVOR_NAME_KEY] = "test-flavor"
        os.environ[DSP_FLAVOR_VERSION_KEY] = "1.0"

    def _assert_flavor_reference(self, mqtt_client):
        self.assertEqual(json.loads(mqtt_client.msg)["flavorReference"]["principal"], "tiki-test")
        self.assertEqual(json.loads(mqtt_client.msg)["flavorReference"]["name"], "test-project")
        self.assertEqual(json.loads(mqtt_client.msg)["flavorReference"]["environment"], "test-env")
        self.assertEqual(json.loads(mqtt_client.msg)["flavorReference"]["flavorName"], "test-flavor")
        self.assertEqual(json.loads(mqtt_client.msg)["flavorReference"]["flavorVersion"], "1.0")
