import unittest

import os
from ai_common import setup_logger
from ai_common import get_log_level


class LoggingTest(unittest.TestCase):

    def test_setup_logger(self):
        os.environ["LOG_LEVEL"] = "DEBUG"
        setup_logger("LOG_LEVEL")
        self.assertEqual(get_log_level(), "DEBUG")

