import unittest

from ai_common import JwtContext


class JwtContextTest(unittest.TestCase):

    def test_jwt_context_initialization(self):
        context = JwtContext('https://auth.tiki-dsp.io/auth/realms/tiki')
        self.assertEqual(context.jwks_uri, 'https://auth.tiki-dsp.io/auth/realms/tiki/protocol/openid-connect/certs')
        self.assertIsNotNone(context.validator, 'Validator may not be none')

