import logging
import os
from enum import Enum

import paho.mqtt.publish as publish

logger = logging.getLogger(__name__)


class MqttTopics:
    REGISTER_ASYNC = "qos/register/async"
    RESULT_ASYNC = "qos/result/async"
    RESULT_DYNAMIC = "qos/result/dynamic"


class MqttQos(Enum):
    AT_MOST_ONCE = 0
    AT_LEAST_ONCE = 1
    EXACTLY_ONCE = 2


class IMqttClient:
    def publish(self, topic: str, msg: str, qos: MqttQos = MqttQos.AT_MOST_ONCE):
        pass


class MqttClient(IMqttClient):
    PORT_ENV = 'NAMESPACE_BROKER_SERVICE_PORT'
    broker_port = 1883
    broker_host_name = "namespace-broker"

    def __init__(self):
        if self.PORT_ENV in os.environ:
            self.broker_port = os.environ[self.PORT_ENV]
        else:
            logger.warning(f"env var {self.PORT_ENV} is not set, it will be set to default value: {self.broker_port}")

    def publish(self, topic: str, msg: str, qos: MqttQos = MqttQos.AT_MOST_ONCE):
        publish.single(topic, msg, hostname=self.broker_host_name, port=int(self.broker_port), qos=qos.value)
