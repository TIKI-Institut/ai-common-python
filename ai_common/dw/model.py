import json


# [{"name":"elolist","size":2,"isDir":true,"fsType":"file","lastModified":1625655259}]
class ListResult:
    def __init__(self, name, isDir, fsType, lastModified, **kwargs):
        """
        - kwargs is in argument list to overcome errors if result of list action returns more than the expected keywords
        - "size" is not in ListResult, since it is returned by pvc list actions but not by hdfs list actions.
        """
        self.name = name
        self.is_dir = isDir
        self.fs_type = fsType
        self.last_modified = lastModified

    @classmethod
    def from_json(cls, json_str):
        json_dict = json.loads(json_str)
        return cls(**json_dict)


# {"taskId":6,"operation":"MOVE","status":"DONE","from":"hdfs:/text.txt","to":"hdfs:/new.txt","username":"tiki-tfrischholz","principal":"tiki-test","created":1627297701}
class TaskResult:
    def __init__(self, taskId, operation, status, from_, to, overwrite, username, principal, created):
        self.task_id = taskId
        self.operation = operation
        self.status = status
        self.from_ = from_
        self.to = to
        self.overwrite = overwrite
        self.username = username
        self.principal = principal
        self.created = created

    @classmethod
    def from_json(cls, json_str):
        json_dict = json.loads(json_str)
        return cls(**json_dict)

    # Converting key "from" because response from wb client contains python keyword "from",
    # which cannot be use as an argument and field name
    @classmethod
    def from_dict(cls, response_dict):
        if 'from' in response_dict.keys():
            response_dict['from_'] = response_dict.pop('from')
        return cls(**response_dict)
