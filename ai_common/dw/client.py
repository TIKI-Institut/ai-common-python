import json
import logging
from typing import Optional

import requests
import time
from requests_toolbelt import MultipartEncoder

from .model import ListResult, TaskResult

logger = logging.getLogger(__name__)


class DWClient:
    def __init__(self, service_url, token_path, wait_timeout=10):
        self.service_url = service_url
        self.token_path = token_path
        self.wait_timeout = wait_timeout

    def __token(self):
        with open(self.token_path) as reader:
            return reader.read()

    def __bearer_token(self):
        return "Bearer %s" % self.__token()

    def __default_headers(self):
        return {
            'Authorization': self.__bearer_token(),
        }

    @staticmethod
    def __operation_payload(*args):
        if len(args) == 1:
            return json.dumps({'uri': args[0]})
        if len(args) == 4:
            return json.dumps({'from': args[0], 'to': args[1], 'overwrite': args[2], 'wait': args[3]})

    def __get(self, url, headers):
        default_header = self.__default_headers()
        final_headers = {**default_header, **headers}
        return requests.request("GET", url, headers=final_headers)

    def __post(self, url, headers, payload):
        default_header = self.__default_headers()
        final_headers = {**default_header, **headers}

        return requests.request("POST", url, headers=final_headers, data=payload)

    # returns ["pvc","hdfs","file"] or None when error occurs
    def fs(self) -> [str]:
        response = self.__get(self.service_url + '/fs', {'Content-Type': 'application/json'})
        try:
            response.raise_for_status()
            return response.json()

        except requests.exceptions.HTTPError as error:
            logger.error(error)

    def list(self, source: str):
        payload = self.__operation_payload(source)
        response = self.__post(self.service_url + '/list', {'Content-Type': 'application/json'}, payload)
        try:
            response.raise_for_status()
            response_list = []

            for obj in response.json():
                response_list.append(ListResult(**obj))

            return response_list

        except requests.exceptions.HTTPError as error:
            logger.error(error)

    def delete(self, source: str):
        payload = self.__operation_payload(source)
        response = self.__post(self.service_url + '/delete', {'Content-Type': 'application/json'}, payload)
        try:
            response.raise_for_status()
            return True

        except requests.exceptions.HTTPError as error:
            logger.error(error)
            return False

    def __wait_for_result(self, task_result: TaskResult) -> TaskResult:

        if task_result.status == 'DONE':
            return task_result

        if task_result.status == 'CREATED' or task_result.status == 'RUNNING':
            # wait for the configured wait timeout to ask for processing status
            time.sleep(self.wait_timeout)
            new_task = self.task_status(task_result.task_id)

            if new_task is None:
                raise Exception('error on retrieving task with id %d' % task_result.task_id)

            return self.__wait_for_result(new_task)

        raise Exception('unexpected task status %s for task with id %d', (task_result.status, task_result.task_id))

    def copy(self, from_: str, to: str, overwrite: bool, wait: bool) -> Optional[TaskResult]:
        payload = self.__operation_payload(from_, to, overwrite, wait)
        response = self.__post(self.service_url + '/copy', {'Content-Type': 'application/json'}, payload)
        try:
            response.raise_for_status()
            task_result = TaskResult.from_dict(response.json())

            if wait:
                return self.__wait_for_result(task_result)
            else:
                return task_result

        except requests.exceptions.HTTPError as error:
            logger.error(error)
            return None

    def move(self, from_: str, to: str, overwrite: bool, wait: bool):
        payload = self.__operation_payload(from_, to, overwrite, wait)
        response = self.__post(self.service_url + '/move', {'Content-Type': 'application/json'}, payload)
        try:
            response.raise_for_status()
            task_result = TaskResult.from_dict(response.json())

            if wait:
                return self.__wait_for_result(task_result)
            else:
                return task_result

        except requests.exceptions.HTTPError as error:
            logger.error(error)
            return None

    def task_status(self, task_id: int) -> Optional[TaskResult]:
        response = self.__get(self.service_url + '/task/%d' % task_id, {'Content-Type': 'application/json'})
        try:
            response.raise_for_status()
            task_result = TaskResult.from_dict(response.json())

            return task_result

        except requests.exceptions.HTTPError as error:
            logger.error(error)
            return None

    def download(self, source: str) -> Optional[bytes]:
        payload = self.__operation_payload(source)
        response = self.__post(self.service_url + '/download', {'Content-Type': 'application/json'}, payload)
        try:
            response.raise_for_status()
            return response.content

        except requests.exceptions.HTTPError as error:
            logger.error(error)
            return None

    def upload(self, to: str, file: str, overwrite: bool = False) -> bool:

        try:
            with open(file, 'rb') as handle:
                m = MultipartEncoder(
                    fields={
                        'to': to,
                        'files': (file, handle),
                        'overwrite': str(overwrite)
                    }
                )

                response = self.__post(self.service_url + '/upload', {'Content-Type': m.content_type}, payload=m)
                response.raise_for_status()
                return True

        except requests.exceptions.HTTPError as error:
            logger.error(error)
            return False
        except Exception as e:
            logger.error(e)
            return False

    def mk_dir(self, source: str) -> bool:
        payload = self.__operation_payload(source)
        response = self.__post(self.service_url + '/mkdir', {'Content-Type': 'application/json'}, payload)
        try:
            response.raise_for_status()
            return True

        except requests.exceptions.HTTPError as error:
            logger.error(error)
            return False
