import asyncio
import json
import logging
from datetime import datetime

import jsonpickle

from ai_common.mqtt.mqttClient import MqttClient, MqttTopics
from ai_common.qos.model.flavorReference import FlavorReference
from ai_common.qos.model.qosEntry import QosEntry, QosEntryType
from ai_common.qos.model.qosRunResult import QosRunStatus, QosRunResult

logger = logging.getLogger(__name__)

_mqtt_client = MqttClient()
_lock = asyncio.Lock()


def _set_optional(optional_param: str, flavor_attribute_name: str, flavor: FlavorReference):
    if optional_param:
        setattr(flavor, flavor_attribute_name, optional_param)
    if not getattr(flavor, flavor_attribute_name):
        setattr(flavor, flavor_attribute_name, "")


def save_dynamic_score(entry_name: str, score: float, parameters: dict = {}, project: str = None,
                       environment: str = None,
                       flavor_name: str = None,
                       flavor_version: str = None):
    entry = QosEntry(entry_name, QosEntryType.DYNAMIC)
    flavor = FlavorReference.create_flavor_reference()
    _set_optional(project, "name", flavor)
    _set_optional(environment, "environment", flavor)
    _set_optional(flavor_name, "flavorName", flavor)
    _set_optional(flavor_version, "flavorVersion", flavor)

    result = QosRunResult(0,
                          datetime.now(),
                          QosRunStatus.SUCCESS.value,
                          f"QoS run for {entry.name} completed",
                          json.dumps(parameters),
                          score,
                          flavor,
                          None,
                          entry)
    _mqtt_client.publish(MqttTopics.RESULT_DYNAMIC,
                         jsonpickle.encode(result, unpicklable=False))
