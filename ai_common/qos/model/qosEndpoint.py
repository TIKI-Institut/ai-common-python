from dataclasses import dataclass
from typing import List

import jsonpickle

from ai_common.qos.model.flavorReference import FlavorReference
from ai_common.qos.model.podReference import PodReference
from ai_common.qos.model.qosEntry import QosEntry


@dataclass(frozen=True)
class QosEndpoint:
    entries: List[QosEntry]
    flavorReference: FlavorReference
    podReference: PodReference

    @staticmethod
    def serialize(endpoint):
        return jsonpickle.encode(endpoint, unpicklable=False)
