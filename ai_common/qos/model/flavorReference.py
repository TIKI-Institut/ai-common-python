import os

DSP_PRINCIPAL_KEY = "DSP_PRINCIPAL"
DSP_NAME_KEY = "DSP_NAME"
DSP_ENVIRONMENT_KEY = "DSP_ENVIRONMENT"
DSP_FLAVOR_NAME_KEY = "DSP_FLAVOR"
DSP_FLAVOR_VERSION_KEY = "DSP_FLAVOR_VERSION"


class FlavorReference:
    name: str
    principal: str
    environment: str
    flavorName: str
    flavorVersion: str

    @staticmethod
    def create_flavor_reference():
        flavor = FlavorReference()
        flavor.principal = os.environ[DSP_PRINCIPAL_KEY]
        if not flavor.principal:
            raise ValueError("principal is mandatory but was not provided")
        flavor.name = os.environ.get(DSP_NAME_KEY)
        if not flavor.name:
            raise ValueError("project name is mandatory but was not provided")
        flavor.environment = os.environ.get(DSP_ENVIRONMENT_KEY)
        flavor.flavorName = os.environ.get(DSP_FLAVOR_NAME_KEY)
        flavor.flavorVersion = os.environ.get(DSP_FLAVOR_VERSION_KEY)
        return flavor
