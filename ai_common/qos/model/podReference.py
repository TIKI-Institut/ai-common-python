import os


class PodReference:
    namespace: str
    pod: str

    @staticmethod
    def current_pod():
        pod = PodReference()
        pod.namespace = os.environ.get("DSP_NAMESPACE")
        pod.pod = os.environ.get("DSP_POD")
        return pod
