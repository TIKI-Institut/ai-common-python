from enum import Enum


class QosEntryType(Enum):
    ASYNC = "Async"
    SYNC = "Sync"
    DYNAMIC = "Dynamic"


class QosEntry:

    def __init__(self, name: str, entry_type: QosEntryType):
        self.name = name
        self.type = entry_type.value
