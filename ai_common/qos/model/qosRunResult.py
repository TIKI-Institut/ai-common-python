from datetime import datetime
from enum import Enum
from typing import Optional

from dataclasses import dataclass

from ai_common.qos.model.flavorReference import FlavorReference
from ai_common.qos.model.podReference import PodReference
from ai_common.qos.model.qosEntry import QosEntry


class QosRunStatus(Enum):
    SUCCESS = "Success"
    FAILED = "Failed"
    NOT_FOUND = "NotFound"
    RUNNING = "Running"


@dataclass(frozen=True)
class QosRunResult:
    id: int
    producedOn: datetime
    status: QosRunStatus
    statusMessage: str
    parameters: str
    score: Optional[float]
    flavorReference: FlavorReference
    podReference: PodReference
    qosEntry: Optional[QosEntry]
