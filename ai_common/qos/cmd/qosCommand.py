import logging
from datetime import datetime
from typing import Callable, Optional

import jsonpickle

from ai_common.mqtt.mqttClient import MqttClient, MqttTopics, IMqttClient
from ai_common.qos.model.flavorReference import FlavorReference
from ai_common.qos.model.qosEndpoint import QosEndpoint
from ai_common.qos.model.qosEntry import QosEntry, QosEntryType
from ai_common.qos.model.qosRunResult import QosRunResult, QosRunStatus

logger = logging.getLogger(__name__)


class QosCommand:
    def __init__(self, client: IMqttClient = MqttClient()):
        # connect to mqtt later, such that result already is computed, otherwise connection could be broken
        self.mqtt_client = client

    async def register_async_endpoint(self, entries):
        entries = list(map(lambda key: QosEntry(key, QosEntryType.ASYNC), entries))
        endpoint = QosEndpoint(entries, FlavorReference.create_flavor_reference(), None)
        self.mqtt_client.publish(MqttTopics.REGISTER_ASYNC, QosEndpoint.serialize(endpoint))

    async def run_qos_async_job(self, transaction_id: int, entry_name: str, qos_fn: Optional[Callable],
                                parameters: str):
        logger.info(f"qos_run - transactionId:{transaction_id}, entryName: {entry_name}, parameters: {parameters}")

        entry = QosEntry(entry_name, QosEntryType.ASYNC)
        if qos_fn is None:
            logger.warning(f"QoS function for async QoS entry {entry_name} was not found")
            not_found_result = QosRunResult(transaction_id,
                                            datetime.now(),
                                            QosRunStatus.NOT_FOUND.value,
                                            f"QoS function for async QoS entry {entry_name} was not found!",
                                            parameters,
                                            None,
                                            FlavorReference.create_flavor_reference(),
                                            None,
                                            entry)
            self.mqtt_client.publish(MqttTopics.RESULT_ASYNC,
                                     jsonpickle.encode(not_found_result, unpicklable=False))
        else:
            try:
                result = qos_fn()
                success_result = QosRunResult(transaction_id,
                                              datetime.now(),
                                              QosRunStatus.SUCCESS.value,
                                              f"Async QoS run for {entry_name} completed",
                                              parameters,
                                              result,
                                              FlavorReference.create_flavor_reference(),
                                              None,
                                              entry)
                logger.info(f"qos_run - success_result: {jsonpickle.encode(success_result, unpicklable=False)}")
                self.mqtt_client.publish(MqttTopics.RESULT_ASYNC,
                                         jsonpickle.encode(success_result, unpicklable=False))
            except Exception as e:
                logger.warning(
                    f"Running async QoS for id {transaction_id}, name {entry_name} has failed with error: {e}")
                failed_result = QosRunResult(transaction_id,
                                             datetime.now(),
                                             QosRunStatus.FAILED.value,
                                             f"Running async QoS for id {transaction_id}, name {entry_name} has failed",
                                             parameters,
                                             None,
                                             FlavorReference.create_flavor_reference(),
                                             None,
                                             entry)
                self.mqtt_client.publish(MqttTopics.RESULT_ASYNC,
                                         jsonpickle.encode(failed_result, unpicklable=False))
