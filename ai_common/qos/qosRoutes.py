import logging
from datetime import datetime

import jsonpickle
from flask import request

from ai_common.qos.model.flavorReference import FlavorReference
from ai_common.qos.model.podReference import PodReference
from ai_common.qos.model.qosEndpoint import QosEndpoint
from ai_common.qos.model.qosEntry import QosEntry, QosEntryType
from ai_common.qos.model.qosRunResult import QosRunResult, QosRunStatus

logger = logging.getLogger(__name__)

QOS_RESOURCES_STORE = {}
QOS_PARAMETERS_STORE = {}
QOS_LIST_URL = "/qos/list"
QOS_RUN_URL = "/qos/run"

"""
   Endpoints which handles qos requests.

   Currently there are two qos endpoints defined:

    qos/run - execute sync qos run, this endpoint is invoked by ai-qos-manager, which run in sidecar container of current web deployment.
    qos/list - list all declared sync entries, this endpoint is invoked also by ai-qos-manager
"""


def qos_list_endpoint():
    entries = list(map(lambda key: QosEntry(key, QosEntryType.SYNC), QOS_RESOURCES_STORE.keys()))
    endpoint = QosEndpoint(entries=entries, flavorReference=FlavorReference.create_flavor_reference(),
                           podReference=PodReference.current_pod())
    serialized = jsonpickle.encode(endpoint, unpicklable=False)
    logger.info(f"serialized: {serialized}")
    return serialized


def qos_run_endpoint():
    transaction_id_param = request.values.get("transactionId")
    if request.values.get("transactionId") is None:
        return f"Missing transactionId request parameter", 400
    try:
        transaction_id = int(transaction_id_param)
    except Exception as e:
        msg = f"Invalid transactionId: {transaction_id_param}"
        logger.warning(e)
        logger.warning(msg)
        return msg, 400

    entry_name = request.values.get("name")
    if entry_name is None:
        return f"Missing name request parameter", 400

    qos_fn = QOS_RESOURCES_STORE.get(entry_name)
    parameters = QOS_PARAMETERS_STORE.get(entry_name)
    if qos_fn is None:
        logger.warning(f"Sync QoS entry {entry_name} was not found")
        return jsonpickle.encode(QosRunResult(transaction_id,
                                              datetime.now(),
                                              QosRunStatus.NOT_FOUND.value,
                                              f"Sync QoS entry {entry_name} was not found",
                                              parameters,
                                              None,
                                              FlavorReference.create_flavor_reference(),
                                              PodReference.current_pod(),
                                              None), unpicklable=False)

    try:
        result = qos_fn()
        return jsonpickle.encode(QosRunResult(transaction_id,
                                              datetime.now(),
                                              QosRunStatus.SUCCESS.value,
                                              f"Sync QoS run for {entry_name} completed",
                                              parameters,
                                              result,
                                              FlavorReference.create_flavor_reference(),
                                              PodReference.current_pod(),
                                              QosEntry(entry_name, QosEntryType.SYNC)), unpicklable=False)
    except Exception as e:
        msg = f"Running sync QoS for id {transaction_id}, name {entry_name} has failed"
        logger.warning(msg)
        logger.warning(e)
        return jsonpickle.encode(QosRunResult(transaction_id,
                                              datetime.now(),
                                              QosRunStatus.FAILED.value,
                                              msg,
                                              parameters,
                                              None,
                                              FlavorReference.create_flavor_reference(),
                                              PodReference.current_pod(),
                                              QosEntry(entry_name, QosEntryType.SYNC)), unpicklable=False)
