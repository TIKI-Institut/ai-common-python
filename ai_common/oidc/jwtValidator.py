import json
import urllib.request as request
from datetime import datetime

from jwkest import BadSignature
from jwkest.jwk import KEYS
from jwkest.jws import JWS

from ai_common.oidc.common import NoAuthorizationError
from ai_common.oidc.tools import base64_urldecode
from ai_common.oidc.tools import get_ssl_context


class JwtValidator:
    def __init__(self, config):
        self.ctx = get_ssl_context(config)

        self.jwks_uri = config['jwks_uri']
        self.jwks = self.load_keys()

    def unpack(self, jwt):
        parts = jwt.split('.')
        if len(parts) != 3:
            raise BadSignature('Invalid JWT. Only JWS supported.')
        header = json.loads(base64_urldecode(parts[0]))
        payload = json.loads(base64_urldecode(parts[1]))

        return header, payload

    def validate(self, jwt, iss, aud):
        header, payload = self.unpack(jwt)

        if iss != payload['iss']:
            raise NoAuthorizationError("Invalid issuer %s, expected %s" % (iss, payload['iss']))

        if payload["aud"]:
            if (isinstance(payload["aud"], str) and payload["aud"] != aud) or aud not in payload['aud']:
                raise NoAuthorizationError("Invalid audience %s, expected %s" % (aud, payload['aud']))

        if not payload['exp']:
            raise NoAuthorizationError("No expiry timestamp given in token")

        expiry_date = datetime.utcfromtimestamp(payload['exp'])
        if expiry_date < datetime.utcnow():
            raise NoAuthorizationError("Token is expired (%s)" % expiry_date)

        jws = JWS(alg=header['alg'])
        try:
            jws.verify_compact(jwt, self.jwks)
        except Exception as e:
            raise NoAuthorizationError(e)

        return True

    def get_jwks_data(self):
        jwtConfigRequest = request.Request(self.jwks_uri)
        jwtConfigRequest.add_header('Accept', 'application/json')
        jwtConfigRequest.add_header('User-Agent', 'CurityExample/1.0')

        try:
            jwks_response = request.urlopen(jwtConfigRequest, context=self.ctx)
        except Exception as e:
            raise e
        return jwks_response.read().decode('utf-8')

    def load_keys(self):
        # load the jwk set.
        jwks = KEYS()
        jwks.load_jwks(self.get_jwks_data())
        return jwks
