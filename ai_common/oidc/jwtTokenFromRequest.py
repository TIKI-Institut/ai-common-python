from ai_common.oidc.common import NoAuthorizationError

#KEEP IN MIND:
#all *TokenContext classes have to have the methods 'token', 'store_to_context', 'get_from_context'

class RequestTokenContext:
    import flask

    UNPROTECTED_HTTP_METHODS = ['OPTIONS']

    JWT_HEADER_NAME = 'Authorization'
    JWT_HEADER_TYPE = 'Bearer'

    def _decode_jwt_from_headers(self):
        # Verify we have the auth header
        jwt_header = self.flask.request.headers.get(self.JWT_HEADER_NAME, None)

        if not jwt_header:
            raise NoAuthorizationError("Missing %s Header" % self.JWT_HEADER_NAME)

        # Make sure the header is in a valid format that we are expecting, ie
        # <HeaderName>: <HeaderType(optional)> <JWT>
        parts = jwt_header.split()
        if not self.JWT_HEADER_TYPE:
            if len(parts) != 1:
                raise NoAuthorizationError("Bad %s header. Expected value '<JWT>'" % self.JWT_HEADER_NAME)

            encoded_token = parts[0]
        else:
            if parts[0] != self.JWT_HEADER_TYPE or len(parts) != 2:
                raise NoAuthorizationError("Bad %s header. Expected value '%s <JWT>'" % (self.JWT_HEADER_NAME, self.JWT_HEADER_TYPE))

            encoded_token = parts[1]

        return encoded_token


    def token(self):
        if self.flask.request.method not in self.UNPROTECTED_HTTP_METHODS:
            return self._decode_jwt_from_headers()

        return None

    def store_to_context(self, name, object):
        setattr(self.flask.g, name, object)

    def get_from_context(self, name):
        if not hasattr(self.flask.g, name):
            return None

        return getattr(self.flask.g, name)

