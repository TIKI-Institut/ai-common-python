import base64
import random
import string
import ssl


def base64_urldecode(s):
    ascii_string = str(s)
    ascii_string += '=' * (4 - (len(ascii_string) % 4))
    return base64.urlsafe_b64decode(ascii_string)


def base64_urlencode(s):
    return base64.urlsafe_b64encode(s).split("=")[0].replace('+', '-').replace('/', '_')


def decode_token(token):
    """
    Decode a jwt into readable format.
    :param token:
    :return: A decoded jwt, or None if its not a JWT
    """
    parts = token.split('.')

    if token and len(parts) == 3:
        return base64_urldecode(parts[0]), base64_urldecode(parts[1])

    return None


def generate_random_string(size=20):
    """
    :return: a random string with a default size of 20 bytes using only ascii characters and digits
    """
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(size))


def get_ssl_context(config):
    """
    :return a ssl context with verify and hostnames settings
    """
    ctx = ssl.create_default_context()

    if 'verify_ssl_server' in config and not bool(config['verify_ssl_server']):
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE

    return ctx

