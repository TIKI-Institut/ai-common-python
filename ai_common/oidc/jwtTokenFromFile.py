import os
import time
from ai_common.oidc.common import NoAuthorizationError

#KEEP IN MIND:
#all *TokenContext classes have to have the methods 'token', 'store_to_context', 'get_from_context'

class FileTokenContext():
    def __init__(self, file):
        self._file = file
        self._store = {}
        self._firstAccess = True
        self._tokenFileTimeout = 5

    def token(self):
        try:
            if not os.path.exists(self._file):
                while self._tokenFileTimeout >= 0:
                    time.sleep(1)
                    self._tokenFileTimeout = self._tokenFileTimeout - 1
                    if os.path.exists(self._file):
                        break

            if not os.path.exists(self._file):
                raise Exception("Timout for reading token file %s" % self._file)

            with open(self._file, 'r') as inFile:
                return inFile.read()

        except Exception as e:
            raise NoAuthorizationError("Unable to read token from file %s (%s)" % (self._file, str(e)))

    def store_to_context(self, name, object):
        self._store[name] = object

    def get_from_context(self, name):
        return self._store[name]

