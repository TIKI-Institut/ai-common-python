from ai_common.auth.common import AiNotAuthorizedException


class JwtToken:
    def __init__(self, payload):
        self.payload = payload

    @property
    def Issuer(self):
        return self.payload["iss"]

    @property
    def Subject(self):
        return self.payload["sub"]

    @property
    def Audience(self):
        return self.payload["aud"]

    @property
    def ExpirationTime(self):
        return self.payload["exp"]

    @property
    def NotBefore(self):
        return self.payload["nbf"]

    @property
    def IssuedAt(self):
        return self.payload["iat"]

    @property
    def JwtID(self):
        return self.payload["jti"]

    @property
    def ResourceAccess(self):
        return self.payload["resource_access"]

    def Claim(self, claim):
        return self.payload[claim]

    def Roles(self, client):
        try:
            if self.ResourceAccess[client]:
                if self.ResourceAccess[client]["roles"]:
                    return self.ResourceAccess[client]["roles"]
        except Exception as e:
            raise AiNotAuthorizedException("Unable to read roles")
