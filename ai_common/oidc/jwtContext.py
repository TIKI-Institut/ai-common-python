from ai_common.oidc.jwtValidator import JwtValidator


class JwtContext:
    def __init__(self, issuer):
        self.issuer = issuer
        self.jwks_uri = issuer + '/protocol/openid-connect/certs'
        self.validator = JwtValidator({'jwks_uri': self.jwks_uri})

    def validate(self, token, forClient):
        return self.validator.validate(token, self.issuer, forClient)

    def unpack(self, token):
        _, payload = self.validator.unpack(token)
        return payload
