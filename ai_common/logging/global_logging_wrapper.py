import logging
import os

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s: %(message)s', datefmt='%d/%m/%Y %H:%M:%S')
globalLogger = logging.getLogger()


def setup_logger(env: str):
    env_log_level: str = os.getenv(env)
    try:
        set_log_level(logging.getLevelName(env_log_level))
        log_info("setting log level to {level}".format(level=env_log_level))
    except ValueError as error:
        log_warn("Received Invalid level '{level}'. Will continue with default level INFO".format(level=error))
        set_log_level(logging.INFO)


def get_logger_instance(name: str):
    return logging.getLogger(name)


def set_log_level(level: int):
    globalLogger.setLevel(level)


def get_log_level() -> str:
    return logging.getLevelName(globalLogger.getEffectiveLevel())


def log_debug(message: str):
    globalLogger.debug(message)


def log_info(message: str):
    globalLogger.info(message)


def log_warn(message: str):
    globalLogger.warning(message)


def log_error(message: str):
    globalLogger.error(message)


def log_exception(e: Exception):
    globalLogger.exception(e)


def add_logging_handler(handler: logging.Handler):
    globalLogger.addHandler(handler)
