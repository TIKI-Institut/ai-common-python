# -*- coding: utf-8 -*-
import json

from ai_common.auth import need_roles
from ai_common.qos.qosRoutes import QOS_RESOURCES_STORE, QOS_PARAMETERS_STORE

"""AI Common Web Endpoint Decorator

This decorator has the following features:

* Store specific client endpoints for later propagation in a python web framework
* Store client roles to secure the endpoint
* Store a reference to a "naked" function which will be processed by an endpoint

This library targets "Convention over Configuration"
Usage:

@ai_common.web_resource('open', None, 'GET')
def open_endpoint():
    return "This is an open endpoint with no login required"


@ai_common.web_resource('secured', [], 'GET')
def secured():
    return "This endpoint need a login"


@ai_common.web_resource('role_secured', ['needLogin', 'needLogin3'], 'GET')
def role_secured():
    return "This endpoint need all roles"

"""
WEB_RESOURCES_STORE = {}
WEB_RESOURCE_KEY_ENDPOINT = 'endpoint'
WEB_RESOURCE_KEY_METHODS = 'methods'
WEB_RESOURCE_KEY_FUNCTION = 'fn'


def sync_qos_entry(entry_name, parameters: dict = {}):
    def qos_decorator(fn):
        QOS_RESOURCES_STORE[entry_name] = fn
        QOS_PARAMETERS_STORE[entry_name] = json.dumps(parameters)

    return qos_decorator


def web_resource(endpoint: str, roles: [str], methods: [str] = None):
    if methods is None:
        methods = ['GET']

    def wrapper(fn):
        wrapped_function = need_roles(roles)(fn)

        WEB_RESOURCES_STORE[endpoint] = \
            {
                WEB_RESOURCE_KEY_ENDPOINT: endpoint,
                WEB_RESOURCE_KEY_METHODS: methods,
                WEB_RESOURCE_KEY_FUNCTION: wrapped_function
            }

        return wrapped_function

    return wrapper


"""AI Common Web Application Startup Decorator

This decorator is only required in web applications. Its target is to run
code before the flask web server is running. (e.g. for background logic)

This decorator has the following features:

* Store a reference to a functions that has to be called before starting the web server

@ai_common.web_application_startup
def on_application_startup():
    ...custom logic here....

"""
WEB_APPLICATION_STARTUP_FUNCTION_STORE = []
WEB_APPLICATION_STARTUP_KEY_FUNCTION = 'fn'


def web_application_startup():
    def wrapper(fn):
        WEB_APPLICATION_STARTUP_FUNCTION_STORE.append(fn)
        return fn

    return wrapper
