import asyncio
import errno
import json
import logging
import os
import sys
from functools import wraps

import click

from ai_common.auth import need_roles, dump_roles
from ai_common.qos.cmd.qosCommand import QosCommand


logger = logging.getLogger(__name__)
JOB_STORE = {}

JOB_KEY_MAIN = 'main'
JOB_KEY_INIT_FUNCTION = 'init'
JOB_KEY_QOS_FUNCTION = 'qos'
JOB_KEY_QOS_FUNCTION_PARAMS = 'qos-params'

JOB_STORE[JOB_KEY_MAIN] = None
JOB_STORE[JOB_KEY_INIT_FUNCTION] = []

""" Dictionary with job entries and qos functions i.e:
    { "entry1": entry1_fn,
      "entry2": entry2_fn
        ...
    } """
JOB_STORE[JOB_KEY_QOS_FUNCTION] = {}
JOB_STORE[JOB_KEY_QOS_FUNCTION_PARAMS] = {}


def job(name: str, roles: [str] = None, **kwargs):
    # -*- coding: utf-8 -*-
    """AI Common Job Decorator

    This decorator has the following features:

    * Registers a specific job as command in click CLI argument parsing framework
    * Store client roles to secure this function call
    * Store a reference to a "naked" function which will be called later

    This library targets "Convention over Configuration"
    Usage:

    @ai_common.job("blub", None)
    def open_job():
        return "This is a open command named 'blub' which does not need a login"


    @ai_common.job("foo", [])
    def secured_job():
        return "This open command named 'foo' needs the current executing user to have a valid login"


    @ai_common.job("bar", ['needLogin', 'needLogin3'])
    def role_secured():
        return "This open command named 'bar' needs the current executing user to have a valid login and have all the mentioned permissions"


    @ai_common.job("bar", ['needLogin', 'needLogin3'], short_help='Some additional Info')
    def role_secured():
        return "This open command named 'bar' needs the current executing user to have a valid login and have all the mentioned permissions"

    it is possible to add more information to the command registration.
    See :  https://click.palletsprojects.com/en/7.x/documentation/ for reference information

    """

    def wrapper(fn):

        @wraps(fn)
        def _partial(*args, **_kwargs):
            for init_func in JOB_STORE[JOB_KEY_INIT_FUNCTION]:
                init_func()

            # Click-framework absorbs return values. This will impact the result state of the kubernetes pod in that way
            # that its always "Completed". Therefore, we need to follow a hard exit strategy with sys.exit.
            result_value = fn(*args, **_kwargs)
            if result_value != 0:
                sys.exit(result_value)
            return result_value

        func = click.command(name, **kwargs)(need_roles(roles)(_partial))

        if JOB_STORE[JOB_KEY_MAIN] is not None:
            # noinspection PyUnresolvedReferences
            JOB_STORE[JOB_KEY_MAIN].add_command(func)

        return func

    return wrapper


def async_qos_entry(entry_name, parameters: dict = {}):
    def qos_decorator(fn):
        JOB_STORE[JOB_KEY_QOS_FUNCTION][entry_name] = fn
        JOB_STORE[JOB_KEY_QOS_FUNCTION_PARAMS][entry_name] = json.dumps(parameters)

    return qos_decorator


def job_init():
    # -*- coding: utf-8 -*-
    """AI Common Job Init Decorator

    This decorator stores a function which will be called just before a jobs get executed
    """

    def wrapper(fn):
        JOB_STORE[JOB_KEY_INIT_FUNCTION].append(fn)
        return fn

    return wrapper


def list_roles(file):
    # -*- coding: utf-8 -*-
    """FOR UNIT TESTS
    """
    json_data = dump_roles()

    if file is not None:
        if not os.path.exists(os.path.dirname(file)):
            try:
                os.makedirs(os.path.dirname(file))
            except OSError as exc:  # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise
        with open(file, "w") as text_file:
            text_file.write(json_data)
    else:
        click.echo(json_data)


@click.command("roles")
@click.option("--file", help='output all roles to file')
def roles_enlistment_command(file):
    list_roles(file)


@click.command("qos-register")
def qos_register():
    """Registers all async QoS endpoints. Executed during 'provision' step of ai-provisioner"""
    entries = JOB_STORE[JOB_KEY_QOS_FUNCTION].keys()
    if not entries:
        logger.info("register_async_endpoint - no entries for registration")
        return
    loop = asyncio.get_event_loop()
    cmd = QosCommand()
    loop.run_until_complete(cmd.register_async_endpoint(entries))


@click.command("qos-run")
@click.argument('transaction_id')
@click.argument('entry_name')
def qos_run(transaction_id, entry_name):
    """Runs a previously declared async QoS endpoint."""
    loop = asyncio.get_event_loop()
    cmd = QosCommand()
    # todo no remove run function, this does not look right
    loop.run_until_complete(
        cmd.run_qos_async_job(transaction_id, entry_name, JOB_STORE[JOB_KEY_QOS_FUNCTION].get(entry_name),
                              JOB_STORE[JOB_KEY_QOS_FUNCTION_PARAMS].get(entry_name)))


def job_main():
    # -*- coding: utf-8 -*-
    """AI Common Job-Main Decorator

    This decorator has the following features:

    * Registers a "main" method as designated entry point for a program
    * Prepares stack for automatic registration of jobs (command)

    This library targets "Convention over Configuration"
    Usage:

    @job_main()
    def main():
        pass

    """

    def wrapper(fn):
        assert JOB_STORE[JOB_KEY_MAIN] is None
        fn = click.group()(fn)
        JOB_STORE[JOB_KEY_MAIN] = fn
        fn.add_command(roles_enlistment_command)
        fn.add_command(qos_register)
        fn.add_command(qos_run)
        return fn

    return wrapper
