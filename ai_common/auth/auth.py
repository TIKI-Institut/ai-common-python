import json
import os
from functools import wraps

from ai_common.logging import log_warn
from ai_common.oidc import JwtContext
from .common import AiNotAuthorizedException

# -*- coding: utf-8 -*-
"""AI Common Auth Decorator

This decorator has the following features:

* Uppon a coll on the decorated function check if the current executing user to has all the mentioned permissions
* Stores the roles for other usages

This library targets "Convention over Configuration"
Usage:

@ai_common.need_roles([])
def secured_function():
    return "This function needs the current executing user to have a valid login"


@ai_common.need_roles(['needLogin', 'needLogin3'])
def role_secured_function():
    return "This funciton needs the current executing user to have a valid login and have all the mentioned permissions"

"""

from ai_common.oidc.jwtToken import JwtToken

AUTH_STORE = {}

AUTH_KEY_ROLES = 'roles'
AUTH_KEY_TOKEN_CONTEXT = 'token-context'
AUTH_KEY_TOKEN_REALM_CONTEXT = 'token-realm-context'

AUTH_KEY_TOKEN = 'dsp-token'

AUTH_STORE[AUTH_KEY_ROLES] = set()


def set_token_context(realm_context, context):
    AUTH_STORE[AUTH_KEY_TOKEN_REALM_CONTEXT] = realm_context
    AUTH_STORE[AUTH_KEY_TOKEN_CONTEXT] = context


def intersection(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3


def current_token():
    tokenContext = AUTH_STORE[AUTH_KEY_TOKEN_CONTEXT]
    return tokenContext.get_from_context(AUTH_KEY_TOKEN)


def need_roles(roles: [str]):
    persist_client_roles(roles)

    def wrapper(fn):
        @wraps(fn)
        def _partial(*args, **kwargs):
            if roles is not None:
                try:
                    if AUTH_STORE[AUTH_KEY_TOKEN_CONTEXT] is None:
                        raise Exception("Token not resolvable")

                    tokenContext = AUTH_STORE[AUTH_KEY_TOKEN_CONTEXT]
                    token = tokenContext.token()

                    if token is None:
                        raise Exception("Token not resolvable")

                    jwt_realm_context = AUTH_STORE[AUTH_KEY_TOKEN_REALM_CONTEXT]

                    if jwt_realm_context is None:
                        raise Exception("Token not resolvable")

                    client_id = os.environ.get("OAUTH_CLIENT_ID")

                    if client_id is None:
                        raise Exception("ENV var 'OAUTH_CLIENT_ID' not set")

                    if not jwt_realm_context.validate(token, client_id):
                        raise Exception("Token is invalid")

                    my_token = JwtToken(jwt_realm_context.unpack(token))

                    tokenContext.store_to_context(AUTH_KEY_TOKEN, my_token)

                    fulfilled_roles = intersection(roles, my_token.Roles(client_id))

                    if len(fulfilled_roles) != len(roles):
                        raise Exception("User is missing roles")

                except Exception as e:
                    raise AiNotAuthorizedException(
                        "The current User is not authorized to execute this call (%s)" % str(e))

            return fn(*args, **kwargs)

        return _partial

    return wrapper


def persist_client_roles(roles: [str]):
    if roles is not None and roles.__len__():
        # Register roles to have a pool of known roles
        AUTH_STORE[AUTH_KEY_ROLES].update(roles)


def dump_roles() -> str:
    json_data = {'roles': list(AUTH_STORE[AUTH_KEY_ROLES])}
    return json.dumps(json_data)


def initialize_jwt_context(context):
    try:
        # OAUTH issuer from provisioner
        issuer = os.environ["JWT_ISSUER"]
        if issuer == "":
            raise ValueError("JWT_ISSUER is empty")
        set_token_context(JwtContext(issuer), context)
    except KeyError:
        # Swallow exception when we want to register roles via [roles --file]
        log_warn(
            "Environment variable [JWT_ISSUER] not set. No token context will be set")
        pass
    except ValueError:
        log_warn(
            "Environment variable [JWT_ISSUER] set to empty string. No token context will be set")
        pass
