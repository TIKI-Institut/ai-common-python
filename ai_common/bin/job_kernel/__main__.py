from ai_common.auth import initialize_jwt_context
from ai_common.decorators import job_main
from ai_common.oidc import FileTokenContext


# THIS DEPLOYMENT GETS A SUPPLY OF ENV VARS TO WORK WITH
#
#
# Main DSP stuff
#  - DSP_PRINCIPAL      the current principal (i.e. tiki)
#  - DSP_NAME           the current deployment name (i.e. ai-examples)
#  - DSP_ENVIRONMENT    the current deployment environment (i.e. python-dev)
#  - DSP_FLAVOR         the current deployment flavor (i.e. webpy36)
#
#
# Security stuff
#  - JWT_ISSUER         the token ISSUER that will be used to validate all provided tokens
#                       (i.e. https://auth.tiki-dsp.io/auth/realms/tiki)
#
#  - OAUTH_CLIENT_ID    the client that will be used to validate all provided tokens
#                       (i.e. ai-examples-python-dev-web-py36)
#
#

@job_main()
def main():
    """
    CLI for start the job or fetch a list of roles
    """
    # setup auth
    initialize_jwt_context(FileTokenContext('/dsp/auth/token'))
    pass


# this ensures import of all stuff....
# noinspection PyUnresolvedReferences
import external

if __name__ == "__main__":
    main()
