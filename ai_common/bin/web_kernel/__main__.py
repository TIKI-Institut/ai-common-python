import logging
import os

import click
import flask
from werkzeug.exceptions import HTTPException
from werkzeug.middleware.proxy_fix import ProxyFix
from waitress import serve

import ai_common
from ai_common.auth import AiNotAuthorizedException, initialize_jwt_context
from ai_common.decorators import job_main, job
from ai_common.logging import log_warn, log_error
from ai_common.oidc import RequestTokenContext
from ai_common.qos.qosRoutes import qos_run_endpoint, qos_list_endpoint, QOS_LIST_URL, QOS_RUN_URL

# THIS DEPLOYMENT GETS A SUPPLY OF ENV VARS TO WORK WITH
#
# DSP deployment stuff
#  - DSP_WEB_DEPLOYMENT_HOST        the deployment host (i.e. subhost.mainhost.com)
#  - DSP_WEB_DEPLOYMENT_PATH        the relative deployment path the host (i.e. /blub/foo/bar)
#
#  => both, DSP_WEB_DEPLOYMENT_HOST and DSP_WEB_DEPLOYMENT_PATH together make up the reachable URL
#     (i.e. https://subhost.mainhost.com/blub/foo/bar )
#
#
# Main DSP stuff
#  - DSP_PRINCIPAL      the current principal (i.e. tiki)
#  - DSP_NAME           the current deployment name (i.e. ai-examples)
#  - DSP_ENVIRONMENT    the current deployment environment (i.e. python-dev)
#  - DSP_FLAVOR         the current deployment flavor (i.e. webpy36)
#
#
# Security stuff
#  - JWT_ISSUER         the token ISSUER that will be used to validate all provided tokens
#                       (i.e. https://auth.tiki-dsp.io/auth/realms/tiki)
#
#  - OAUTH_CLIENT_ID    the client that will be used to validate all provided tokens
#                       (i.e. ai-examples-python-dev-web-py36)
#
#


STORE = ai_common.decorators.WEB_RESOURCES_STORE
STARTUP_FUNCTION_STORE = ai_common.decorators.WEB_APPLICATION_STARTUP_FUNCTION_STORE
K_ENDPOINT = ai_common.decorators.WEB_RESOURCE_KEY_ENDPOINT
K_METHODS = ai_common.decorators.WEB_RESOURCE_KEY_METHODS
K_FUNCTION = ai_common.decorators.WEB_RESOURCE_KEY_FUNCTION

assert STORE is not None
assert K_ENDPOINT is not None
assert K_METHODS is not None
assert K_FUNCTION is not None

app = flask.Flask(__name__, root_path='/app/external', static_url_path='/app/external')

# We need this fix to service https url's to keycloak
app.wsgi_app = ProxyFix(app.wsgi_app)

app.config.setdefault('LOGCONFIG_REQUESTS_LEVEL', logging.INFO)


@job_main()
def main():
    """
    CLI for start the app or fetch a list of roles
    """
    pass


@app.errorhandler(AiNotAuthorizedException)
def handle_user_not_authorized(error):
    log_warn(str(error))
    return '', 401


@app.errorhandler(Exception)
def all_exception_handler(error):
    code = 500
    if isinstance(error, HTTPException):
        code = error.code
    log_error(str(error))
    return '', code


@job("application")
def application():
    # setup auth
    initialize_jwt_context(RequestTokenContext())

    # The web path under this web application is present
    deployment_path = os.environ["DSP_WEB_DEPLOYMENT_PATH"]

    if deployment_path[-1] == '/':
        deployment_path = deployment_path[:-1]

    # Iterate through client registered meta data
    for m in STORE:
        data = STORE[m]
        endpoint = deployment_path + m
        click.echo("Mapping handler for " + m)
        app.add_url_rule(endpoint, endpoint, data[K_FUNCTION],
                         methods=data[K_METHODS])

    app.add_url_rule(QOS_LIST_URL, QOS_LIST_URL, qos_list_endpoint)
    app.add_url_rule(QOS_RUN_URL, QOS_RUN_URL, qos_run_endpoint)

    for startupFunction in STARTUP_FUNCTION_STORE:
        startupFunction()
    serve(app, host="0.0.0.0", port=5000)


# this ensures import of all stuff....
# noinspection PyUnresolvedReferences
import external

if __name__ == "__main__":
    main()
