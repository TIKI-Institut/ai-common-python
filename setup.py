#!/usr/bin/env python3
"""ai-common setup script."""
from setuptools import setup, find_packages

setup(
    name="ai-common",
    version="1.0.0",
    author="TIKI GmbH",
    author_email="info@tiki-institut.com",
    description="AI platform helper library",
    long_description="This library contains code for hosting python applications on the TIKI Data Science Platform",
    license="http://www.apache.org/licenses/LICENSE-2.0",
    url="https://bitbucket.org/TIKI-Institut/ai-common-python",
    packages=find_packages(exclude=["tests"]),
    python_requires='>= 3.10',
    install_requires=[
        # ~= comaptible release operator
        # see https://www.python.org/dev/peps/pep-0440/#compatible-release
        'flask~=2.2.5',
        'click~=8.1.7',
        'pyjwkest~=1.4.2',
        'gmqtt~=0.6.12',
        'jsonpickle~=3.0.2',
        'requests~=2.31.0',
        'requests-toolbelt~=1.0.0',
        'responses~=0.23.3',
        'waitress~=2.1.2',
        'paho-mqtt~=1.6.1'
    ]
)
